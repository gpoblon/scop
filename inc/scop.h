/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:17:17 by gpoblon           #+#    #+#             */
/*   Updated: 2019/02/04 12:09:53 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H

# define SCOP_H
# define GLFW_INCLUDE_GLCOREARB
# define WIN_WIDTH 800
# define WIN_HEIGHT 600
# define WIN_TITLE "SCOP"
# define VERT_OBJ_SHADER_P "libs/obj_shader.vert"
# define VERT_LIGHT_SHADER_P "libs/light_shader.vert"
# define FRAG_OBJ_SHADER_P "libs/obj_shader.frag"
# define FRAG_LIGHT_SHADER_P "libs/light_shader.frag"
# define VBO_OFFSET 9
# define BUFFER_SIZE 45 * VBO_OFFSET
# define FORCE_TEXTURE 0
# define SHOULD_ROTATE 1
# define IS_CULLED 0
# define SHOW_FPS 0

# include <libft.h>
# include "glfw3.h"
# include "glfw3native.h"

typedef struct		s_obj_match
{
	char			*match;
	void			(*f)();
}					t_match;

typedef struct		s_obj_parser
{
	int				*r_ebo;
	int				*s_ebo;
	float			*r_vbo;
	float			*s_vbo;
	unsigned int	r_ebo_size;
	unsigned int	s_ebo_size;
	unsigned int	r_vbo_ipos;
	unsigned int	r_vbo_size;
	unsigned int	s_vbo_size;
	unsigned int	s_vbo_ipos;
	unsigned int	vertex_ipos;
	unsigned int	normal_ipos;
	unsigned int	text_coord_ipos;
	unsigned int	has_texture;
	t_vec3			min;
	t_vec3			max;
}					t_obj_parser;

typedef struct		s_cam
{
	t_vec3			pos;
	t_vec3			to;
	t_vec3			up;
	int				first_calls;
	float			sensivity;
	float			yaw;
	float			pitch;
	float			fov;
}					t_camera;

typedef struct		s_mvp
{
	unsigned int	model_id;
	unsigned int	view_id;
	unsigned int	proj_id;
	t_max4			model;
	t_max4			view;
	t_max4			proj;
}					t_mvp;

typedef struct		s_ambient_light_shader
{
	unsigned int	intensity_id;
	unsigned int	color_id;
	float			intensity;
	t_vec3			color;
}					t_ambient_light;

typedef struct		s_object_shader
{
	t_mvp			mvp;
	unsigned int	shader_id;
	unsigned int	vbo;
	unsigned int	vao;
	unsigned int	ebo;
	unsigned int	size;
	unsigned int	texture_id;
	unsigned int	has_texture;
	unsigned int	has_texture_id;
	unsigned int	color_id;
	unsigned int	pos_id;
	unsigned int	light_shad_color_id;
	unsigned int	intensity_id;
	float			scale_factor;
	float			intensity;
	float			frame_size;
	t_vec3			color;
	t_vec3			pos;
	t_bool			is_rotating;
}					t_obj;

typedef struct		s_scene
{
	t_ambient_light	ambient;
	t_obj			light;
	t_obj			obj;
	t_vec3			view_pos;
	unsigned int	view_pos_id;
	unsigned int	gamma_id;
	unsigned int	draw_size;
	float			gamma;
}					t_scene;

typedef struct		s_env
{
	GLFWwindow		*window;
	t_bool			has_error;
	t_scene			scene;
	t_camera		cam;
	double			delta_time;
	double			last_frame;
}					t_env;

/*
** test
*/
void				print_fl_arr(float *data, int size);
void				print_int_arr(int *data, int size);

/*
** mvp
*/
void				compute_mvp_obj(t_obj *obj, t_camera cam);
void				compute_mvp_light(t_obj *obj, t_camera cam);
void				init_camera(t_camera *cam, float frame_size);
void				init_mvp(t_obj *obj);

/*
** shader
*/
int					build_shader_program(unsigned int *shader_program,
					const char *vert_path, const char *frag_path);
void				update_fragment_color(t_scene *scene, t_vec3 *cam_pos);
void				update_vertex_shader(t_mvp *mvp);

/*
** window
*/
int					glfw_setup(void);
GLFWwindow			*create_window(void);

/*
** gl_buffers
*/
int					setup_scene_data(t_scene *scene, const char *texture_path,
					const char *obj_path);

/*
** lights
*/
void				init_lights(t_scene *sc);

/*
** objects
*/
int					load_obj_file(const char *filepath, t_obj_parser *o);
void				init_obj(t_obj *obj);

/*
** object_match_vec
*/
void				push_vertex(t_obj_parser *o, const char *line);
void				push_normal(t_obj_parser *o, const char *line);
void				push_texture_coord(t_obj_parser *o, const char *line);

/*
** object_match_face
*/
void				push_face(t_obj_parser *o, const char *line);

/*
** object_match_remainder
*/
void				load_mtl(t_obj_parser *o, const char *line);
void				usemtl(t_obj_parser *o, const char *line);
void				do_nothing(t_obj_parser *o, const char *line);

/*
** render
*/
void				render_loop(t_env *env);

/*
** events
*/
void				process_input(t_env *env);

/*
** utils
** void	print_fl_arr(float *data, int size);
** void	print_int_arr(int *data, int size);
*/
float				get_frame_size(t_obj_parser o);
void				exit_proper(char *message, int fd);
void				realloc_vbo_if_needed(float **arr, t_uint ipos,
					t_uint *size, t_uint new_size);

/*
** texture
*/
int					apply_texture(t_obj *obj, const char *filepath);

/*
** string_of_file
*/
char				*string_of_file(const char *filename);

#endif
