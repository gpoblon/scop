NAME		=	scop

SRC			= 	main.c \
				window.c \
				mvp.c \
				gl_buffers.c \
				lights.c \
				obj_match_vec.c \
				obj_match_remainder.c \
				obj_match_face.c \
				objects.c \
				texture.c \
				shader.c \
				render.c \
				events.c \
				test.c \
				utils.c

LFT_D		=	libft/
INC_D		=	inc/
SRC_D		=	src/
OBJ_D		=	obj/
OBJ_P		=	$(addprefix $(OBJ_D), $(SRC:.c=.o))
LIBS_D		=	libs/

FRAMEWORK	=	OpenGL\
            	Cocoa\
            	IOKit\
            	CoreVideo

FRAMEWORKS	=	$(addprefix -framework , $(FRAMEWORK))

CC			=	gcc -g
CFLAGS		=	#-Werror -Wextra -Wall
LDFLAGS		=	-lft -L$(LFT_D) -lglfw3 -L$(LIBS_D) $(FRAMEWORKS)

all: $(OBJ_D) mlft $(NAME)
mlft: $(LFT_D)
	@make -C $(LFT_D)

$(NAME): $(OBJ_P)
	@printf "$(OVERRIDE)$(CYAN)$(PROJECT) | $(GREEN)⌛  source to object files...\t💯 ️ done creating object files$(WHITE)\n"
	@$(CC) $^ -o $@ $(LDFLAGS)
	@printf "$(OVERRIDE)$(CYAN)$(PROJECT) | $(GREEN)㊗️ executable created from object files$(WHITE)\n"

$(OBJ_D)%.o: $(SRC_D)%.c $(INC_D)/scop.h
	@mkdir $(OBJ_D) 2> /dev/null || true
	@$(CC) -c $< -o $@ $(CFLAGS) -I $(INC_D) -I $(LFT_D)$(INC_D)
	@printf "$(OVERRIDE)$(CYAN)$(PROJECT) | $(GREEN)⌛  source to object files... $(YELLOW)%*s$(WHITE)" $(CURSOR_R) "$<"

$(OBJ_D):
	@mkdir -p $(OBJ_D)
	@mkdir -p $(dir $(OBJ_P))
	@echo "$(CYAN)$(PROJECT) | $(GREEN)🆗  object directories created$(WHITE)"

# cosmetic rules
WHITE		=	`echo "\033[0m"`
CYAN		=	`echo "\033[36m"`
GREEN		=	`echo "\033[32m"`
RED			=	`echo "\033[31m"`
YELLOW		=	`echo "\033[33m"`
OVERRIDE	=	`echo "\r\033[K"`
CURSOR_R	=	`echo "$$(tput cols) - 39"|bc`
PROJECT		=	"SCOP"

clean:
	@make -C $(LFT_D) clean
	@rm -rf $(OBJ_D)
	@echo "$(CYAN)$(PROJECT) | clean $(RED)❌  object files cleaned$(WHITE)"

fclean:
	@make -C $(LFT_D) fclean
	@rm -f $(NAME)
	@echo "$(CYAN)$(PROJECT) | fclean $(RED)❌  executable ($(NAME)) cleaned$(WHITE)"
	@rm -rf $(OBJ_D)
	@echo "$(CYAN)$(PROJECT) | clean $(RED)❌  object files cleaned$(WHITE)"

re:	fclean all
	@echo "$(CYAN)$(PROJECT) | re $(YELLOW)♻️  REBUIT$(WHITE)"

.PHONY: all clean fclean re mlft

-include $(OBJ_P:.o=.d)
