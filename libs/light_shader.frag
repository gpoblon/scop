#version 410 core
out vec4 FragColor;

uniform vec3 lColor;

void main()
{
	FragColor = vec4(lColor, 1.0);
}
