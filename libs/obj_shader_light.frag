#version 410 core
out vec4 FragColor;

in vec3 FragPos;
in vec2 TexCoords;
in vec3 Normal;

uniform sampler2D texture0;
uniform int hasTexture;
uniform vec3 oColor;
uniform vec3 aColor;
uniform vec3 lColor;
uniform vec3 viewPos;
uniform vec3 lPos;
uniform float lIntensity;
uniform float aIntensity;
uniform float gamma;

void main()
{
	// FragColor = texture(texture0, TexCoords) * vec4(lighting, 1.0);
	vec3 color;
	if(hasTexture == 0)
	{
		color = oColor;
	}
	else
	{
		color = texture(texture0, TexCoords).rgb;
	}
	FragColor = vec4(pow(color, vec3(1.0/gamma)), 1.0);
}
