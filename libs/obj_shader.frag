#version 410 core
out vec4 FragColor;

in vec3 FragPos;
in vec3 ObjColor;
in vec2 TexCoords;
in vec3 Normal;

uniform sampler2D texture0;
uniform int hasTexture;
uniform vec3 aColor;
uniform vec3 lColor;
uniform vec3 viewPos;
uniform vec3 lPos;
uniform float lIntensity;
uniform float aIntensity;
uniform float gamma;

void main()
{
	// ambient light
	vec3 ambient = aIntensity * aColor;

	// diffuse light
	float distance = length(lPos - FragPos);
	vec3 lightDir = normalize(lPos - FragPos);
	float diff = max(dot(Normal, lightDir), 0.0);
	vec3 diffuse = diff * lIntensity * lColor;

	// specular light
	vec3 specular = vec3(0.0);
	if (diff > 0.0)
	{
		vec3 viewDir = normalize(viewPos - FragPos);
		vec3 halfwayDir = normalize(lightDir + viewDir);
		float spec = pow(max(dot(Normal, halfwayDir), 0.0), 32);
		specular = spec * lIntensity * lColor;
	}

	// gamma correction
	float attenuation = 1.0 / (distance * distance);
	vec3 lighting = (ambient + diffuse * attenuation + specular * attenuation);
	vec3 color;
	if (hasTexture == 0)
	{
		color = ObjColor * lighting;
	}
	else
	{
		vec3 text = texture(texture0, TexCoords).rgb;
		color =  text * lighting;
	}
	FragColor = vec4(pow(color, vec3(1.0/gamma)), 1.0);
}
