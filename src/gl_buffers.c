/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gl_buffers.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 11:52:55 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/30 18:13:07 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void	create_vertex_array(t_uint *vao, t_uint vbo)
{
	glGenVertexArrays(1, vao);
	glBindVertexArray(*vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
}

static void	create_vertex_buffer(float *vertices, t_uint float_count)
{
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * float_count,
		vertices, GL_STATIC_DRAW);
}

static void	bind_buffer_to_array(t_uint offset)
{
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		offset * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
		offset * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
		offset * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);
}

static int	create_object(t_obj *obj, const char *obj_path)
{
	t_obj_parser	obj_parser;

	if (load_obj_file(obj_path, &obj_parser) == -1)
		return (ft_err_print("Failed load object file"));
	obj->has_texture = FORCE_TEXTURE ? FORCE_TEXTURE : obj_parser.has_texture;
	obj->size = obj_parser.s_vbo_ipos / 9;
	obj->frame_size = get_frame_size(obj_parser);
	glGenBuffers(1, &obj->vbo);
	create_vertex_array(&obj->vao, obj->vbo);
	create_vertex_buffer(obj_parser.s_vbo, obj_parser.s_vbo_ipos);
	bind_buffer_to_array(VBO_OFFSET);
	free(obj_parser.r_ebo);
	free(obj_parser.s_ebo);
	free(obj_parser.r_vbo);
	free(obj_parser.s_vbo);
	return (0);
}

int			setup_scene_data(t_scene *scene, const char *texture_path,
			const char *obj_path)
{
	ft_putendl("Parsing object...");
	if (create_object(&scene->obj, obj_path) == -1)
		return (-1);
	ft_putendl("Parsing done.");
	apply_texture(&scene->obj, texture_path);
	if (create_object(&scene->light, "res/light.obj") == -1)
		return (-1);
	scene->obj.is_rotating = SHOULD_ROTATE;
	scene->light.is_rotating = 0;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	return (0);
}
