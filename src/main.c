/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:16:02 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/30 12:39:59 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void	unset_scene(t_scene *scene, char *texture_path, char *obj_path)
{
	glDeleteVertexArrays(1, &scene->obj.vao);
	glDeleteVertexArrays(1, &scene->light.vao);
	glDeleteBuffers(1, &scene->obj.vbo);
	glDeleteBuffers(1, &scene->light.vbo);
	if (scene->obj.texture_id > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
		glDeleteTextures(1, &scene->obj.texture_id);
	}
	free(obj_path);
	free(texture_path);
	glfwTerminate();
}

int			main(int ac, char **av)
{
	t_env	env;
	char	*texture_path;
	char	*obj_path;

	if (ac < 3)
		return (ft_err_print("Usage args: object file + texture required\n"));
	texture_path = ft_strjoin("res/", av[1]);
	obj_path = ft_strjoin("res/", av[2]);
	ft_bzero(&env, sizeof(t_env));
	if (glfw_setup() == -1 || !(env.window = create_window()))
		exit_proper("Failed creating GLFW window (window)", 2);
	if (build_shader_program(&env.scene.obj.shader_id, VERT_OBJ_SHADER_P,
		FRAG_OBJ_SHADER_P) || build_shader_program(&env.scene.light.shader_id,
		VERT_LIGHT_SHADER_P, FRAG_LIGHT_SHADER_P))
		exit_proper("Failed building the shader program (shader)", 2);
	if (setup_scene_data(&env.scene, texture_path, obj_path) == -1)
		exit_proper("Failed building the object (scene)", 2);
	glfwSetInputMode(env.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSwapInterval(0);
	render_loop(&env);
	unset_scene(&env.scene, obj_path, texture_path);
}
