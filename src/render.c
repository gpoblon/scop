/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:16:49 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/31 18:02:16 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void	init(t_scene *scene)
{
	if (IS_CULLED)
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	glEnable(GL_DEPTH_TEST);
	init_mvp(&scene->light);
	init_mvp(&scene->obj);
	init_lights(scene);
	init_obj(&scene->obj);
	scene->view_pos_id =
		glGetUniformLocation(scene->obj.shader_id, "viewPos");
}

static void	print_fps(int *fps, double *tmp_frame, double delta_time)
{
	*tmp_frame += delta_time;
	if (*tmp_frame < 1.0f)
		*fps += 1;
	else
	{
		ft_printf("fps: %d\n", *fps);
		*tmp_frame = 0.0f;
		*fps = 0;
	}
}

static void	draw_target_object(t_env *e)
{
	glUseProgram(e->scene.obj.shader_id);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, e->scene.obj.texture_id);
	compute_mvp_obj(&e->scene.obj, e->cam);
	update_vertex_shader(&e->scene.obj.mvp);
	update_fragment_color(&e->scene, &e->cam.pos);
	glBindVertexArray(e->scene.obj.vao);
	glDrawArrays(GL_TRIANGLES, 0, e->scene.obj.size);
}

static void	draw_light_object(t_env *e)
{
	glUseProgram(e->scene.light.shader_id);
	compute_mvp_light(&e->scene.light, e->cam);
	update_vertex_shader(&e->scene.light.mvp);
	glUniform3fv(e->scene.light.light_shad_color_id, 1,
		(const float *)&e->scene.light.color);
	glBindVertexArray(e->scene.light.vao);
	glDrawArrays(GL_TRIANGLES, 0, e->scene.light.size);
}

void		render_loop(t_env *e)
{
	double		current_frame;
	double		tmp_frame;
	int			fps;

	init_camera(&e->cam, e->scene.obj.frame_size);
	init(&e->scene);
	fps = 0;
	while (!glfwWindowShouldClose(e->window))
	{
		current_frame = glfwGetTime();
		e->delta_time = current_frame - e->last_frame;
		e->last_frame = current_frame;
		if (SHOW_FPS)
			print_fps(&fps, &tmp_frame, e->delta_time);
		process_input(e);
		draw_target_object(e);
		draw_light_object(e);
		glfwSwapBuffers(e->window);
		glfwPollEvents();
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
}
