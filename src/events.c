/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:08:45 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/30 18:06:47 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void	process_cam_look_around(GLFWwindow *window, t_camera *cam,
			float velocity)
{
	double	xpos;
	double	ypos;

	glfwGetCursorPos(window, &xpos, &ypos);
	glfwSetCursorPos(window, WIN_WIDTH / 2, WIN_HEIGHT / 2);
	if (cam->first_calls < 2)
	{
		xpos = (double)(WIN_WIDTH / 2);
		ypos = (double)(WIN_HEIGHT / 2);
		cam->first_calls += 1;
	}
	xpos = xpos - (double)(WIN_WIDTH / 2);
	ypos = (double)(WIN_HEIGHT / 2) - ypos;
	cam->yaw += (float)xpos * velocity * 2;
	cam->pitch += (float)ypos * velocity * 2;
	if (cam->pitch > 75.0f)
		cam->pitch = 75.0f;
	if (cam->pitch < -75.0f)
		cam->pitch = -75.0f;
	cam->to.x = cos(ft_deg_to_rad(cam->pitch)) * sin(ft_deg_to_rad(cam->yaw));
	cam->to.y = sin(ft_deg_to_rad(cam->pitch));
	cam->to.z = -1 * cos(ft_deg_to_rad(cam->pitch))
				* cos(ft_deg_to_rad(cam->yaw));
	ft_vec3normalize(&cam->to, cam->to);
}

static void	process_aux_cam_moove(GLFWwindow *window, t_camera *cam,
			float velocity)
{
	t_vec3	tmp;

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		ft_vec3cross(&tmp, cam->to, cam->up);
		ft_vec3normalize(&tmp, tmp);
		ft_vec3scale(&tmp, tmp, velocity);
		ft_vec3sub(&cam->pos, cam->pos, tmp);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		ft_vec3cross(&tmp, cam->to, cam->up);
		ft_vec3normalize(&tmp, tmp);
		ft_vec3scale(&tmp, tmp, velocity);
		ft_vec3sum(&cam->pos, cam->pos, tmp);
	}
}

static void	process_cam_moove(GLFWwindow *window, t_camera *cam, float velocity)
{
	t_vec3	tmp;

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS
		&& glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		ft_vec3sum(&cam->pos, cam->pos, (t_vec3){0.0f, 0.5f * velocity, 0.0f});
	else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		ft_vec3scale(&tmp, cam->to, velocity);
		ft_vec3sum(&cam->pos, cam->pos, tmp);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS
		&& glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		ft_vec3sub(&cam->pos, cam->pos, (t_vec3){0.0f, 0.5f * velocity, 0.0f});
	else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		ft_vec3scale(&tmp, cam->to, velocity);
		ft_vec3sub(&cam->pos, cam->pos, tmp);
	}
	process_aux_cam_moove(window, cam, velocity);
}

static void	process_light_moove(GLFWwindow *window, t_vec3 *light_pos,
			float velo)
{
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS
		&& glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		ft_vec3sum(light_pos, *light_pos, (t_vec3){0.0f, 0.0f, 0.1f * velo});
	else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		ft_vec3sum(light_pos, *light_pos, (t_vec3){0.0f, 0.1f * velo, 0.0f});
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS
		&& glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		ft_vec3sub(light_pos, *light_pos, (t_vec3){0.0f, 0.0f, 0.1f * velo});
	else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		ft_vec3sub(light_pos, *light_pos, (t_vec3){0.0f, 0.1f * velo, 0.0f});
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		ft_vec3sub(light_pos, *light_pos, (t_vec3){0.1f * velo, 0.0f, 0.0f});
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		ft_vec3sum(light_pos, *light_pos, (t_vec3){0.1f * velo, 0.0f, 0.0f});
}

void		process_input(t_env *env)
{
	GLFWwindow	*win;
	float		velocity;

	win = env->window;
	velocity = 4.0f * (float)env->delta_time;
	if (glfwGetKey(win, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(win, 1);
	if (glfwGetKey(win, GLFW_KEY_1) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	if (glfwGetKey(win, GLFW_KEY_2) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	process_cam_look_around(win, &env->cam, velocity);
	process_cam_moove(win, &env->cam, velocity);
	process_light_moove(win, &env->scene.light.pos, velocity);
}
