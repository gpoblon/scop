/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj_match_remainder.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:21:21 by gpoblon           #+#    #+#             */
/*   Updated: 2018/12/04 11:56:50 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void			load_mtl(t_obj_parser *o, const char *line)
{
	(void)o;
	(void)line;
	return ;
}

void			usemtl(t_obj_parser *o, const char *line)
{
	(void)o;
	(void)line;
	return ;
}

void			do_nothing(t_obj_parser *o, const char *line)
{
	(void)o;
	(void)line;
	return ;
}
