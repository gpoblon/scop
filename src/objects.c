/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objects.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:16:44 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/31 18:10:36 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

t_match		g_match[] =
{
	{"vn"		, &push_normal			},
	{"vt"		, &push_texture_coord	},
	{"v"		, &push_vertex			},
	{"f"		, &push_face			},
	{"mtllib"	, &load_mtl				},
	{"usemtl"	, &usemtl				},
	{"o"		, &do_nothing			},
	{"g"		, &do_nothing			},
	{"s"		, &do_nothing			}
};

static void	init_obj_parser(t_obj_parser *o)
{
	t_uint	buffer_size;

	ft_bzero(&o->min, sizeof(t_vec3));
	ft_bzero(&o->max, sizeof(t_vec3));
	o->has_texture = 0;
	o->vertex_ipos = 0;
	o->text_coord_ipos = 3;
	o->normal_ipos = 6;
	o->r_ebo_size = 0;
	o->s_ebo_size = 0;
	o->r_ebo = NULL;
	o->s_ebo = NULL;
	o->r_vbo_ipos = 0;
	o->s_vbo_ipos = 0;
	buffer_size = BUFFER_SIZE * VBO_OFFSET;
	o->r_vbo_size = buffer_size;
	o->s_vbo_size = buffer_size + buffer_size / 3;
	o->r_vbo = (float *)malloc(buffer_size * sizeof(float));
	o->s_vbo = (float *)malloc((buffer_size + buffer_size / 3) * sizeof(float));
	ft_bzero(o->r_vbo, buffer_size * sizeof(float));
	ft_bzero(o->s_vbo, (buffer_size + buffer_size / 3) * sizeof(float));
}

static int	parse_line(t_obj_parser *o, const char *line)
{
	unsigned int	i;

	i = 0;
	while (i < sizeof(g_match) / sizeof(*g_match) - 1
		&& ft_strncmp(line, g_match[i].match, strlen(g_match[i].match)))
		++i;
	if (o->r_vbo_ipos <= o->vertex_ipos
		|| o->r_vbo_ipos <= o->text_coord_ipos
		|| o->r_vbo_ipos <= o->normal_ipos)
	{
		o->r_vbo_ipos += VBO_OFFSET;
		realloc_vbo_if_needed(&o->r_vbo, o->r_vbo_ipos, &o->r_vbo_size,
			(o->r_vbo_size + BUFFER_SIZE * VBO_OFFSET));
	}
	if (g_match[i].match)
		g_match[i].f(o, line);
	else if (line[0] && line[0] != '#')
		return (ft_err_print(line));
	return (0);
}

static void	recenter_faces(t_obj_parser *o)
{
	unsigned int	i;
	unsigned int	vt_offset;
	t_vec3			center;

	i = 0;
	vt_offset = 3;
	center.x = (o->max.x + o->min.x) / 2.0f;
	center.y = (o->max.y + o->min.y) / 2.0f;
	center.z = (o->max.z + o->min.z) / 2.0f;
	while (i < o->s_vbo_size)
	{
		o->s_vbo[i] -= center.x;
		o->s_vbo[i + 1] -= center.y;
		o->s_vbo[i + 2] -= center.z;
		o->s_vbo[i + vt_offset] = o->s_vbo[i];
		o->s_vbo[i + vt_offset + 1] = o->s_vbo[i + 1];
		o->s_vbo[i + vt_offset + 2] = 0.0f;
		i += 9;
	}
}

int			load_obj_file(const char *filepath, t_obj_parser *o)
{
	int				fd;
	char			*line;
	struct stat		buf;

	line = NULL;
	init_obj_parser(o);
	if ((fd = open(filepath, O_RDONLY)) < 0 || fstat(fd, &buf) < 0)
		return (-1);
	while (get_next_line(fd, &line) > 0)
	{
		if (parse_line(o, line) == -1)
		{
			free(line);
			return (-1);
		}
		free(line);
	}
	recenter_faces(o);
	if (o->s_vbo_ipos == 0)
		return (ft_err_print("Error: object file does not contain faces."));
	return (0);
}

void		init_obj(t_obj *o)
{
	o->intensity = 1.0f;
	o->scale_factor = 1.0f;
	o->has_texture_id = glGetUniformLocation(o->shader_id, "hasTexture");
}
