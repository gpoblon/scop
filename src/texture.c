/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:16:59 by gpoblon           #+#    #+#             */
/*   Updated: 2019/02/04 12:09:19 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void		*load_image_texture(const char *filepath, int *width,
						int *height, size_t *buf_size)
{
	int					fd;
	void				*ptr_f;
	struct stat			buf;
	int					size;

	if ((fd = open(filepath, O_RDONLY)) < 0 ||
		fstat(fd, &buf) < 0 || (ptr_f = mmap(NULL, buf.st_size,
		PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED)
		return (NULL);
	if (((char *)ptr_f)[0] != 'B' || ((char *)ptr_f)[1] != 'M')
		return (NULL);
	size = *(int*)(ptr_f + 34);
	*width = *(int*)(ptr_f + 18);
	*height = *(int*)(ptr_f + 22);
	*buf_size = buf.st_size;
	return (ptr_f);
}

static int		free_loaded_image_texture(void *ptr_mmap, size_t buf_size)
{
	if (munmap(ptr_mmap, buf_size) < 0)
		return (ft_err_print("Munmap texture failed"));
	return (0);
}

static int		generate_texture(const char *filepath)
{
	int				width;
	int				height;
	size_t			buf_size;
	void			*ptr_texture_file;
	unsigned char	*pixel_arr;

	ptr_texture_file = load_image_texture(filepath, &width, &height, &buf_size);
	if (!ptr_texture_file)
		return (ft_err_print("Loading texture failed"));
	pixel_arr = (t_uchar *)(ptr_texture_file + *(int*)(ptr_texture_file + 10));
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, width, height, 0,
		GL_BGR, GL_UNSIGNED_BYTE, pixel_arr);
	glGenerateMipmap(GL_TEXTURE_2D);
	free_loaded_image_texture(ptr_texture_file, buf_size);
	return (0);
}

int				apply_texture(t_obj *obj, const char *filepath)
{
	glUseProgram(obj->shader_id);
	glGenTextures(1, &obj->texture_id);
	glBindTexture(GL_TEXTURE_2D, obj->texture_id);
	if (generate_texture(filepath) == -1)
	{
		obj->has_texture = 0;
		return (-1);
	}
	return (0);
}
