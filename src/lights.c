/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lights.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:07:14 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/29 16:12:38 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void	init_lights(t_scene *sc)
{
	int		shader_id;

	shader_id = sc->obj.shader_id;
	sc->light.pos = (t_vec3){0.0f, 0.0f, 1.0f};
	sc->light.intensity = 0.5f;
	sc->light.color = (t_vec3){1.0f, 1.0f, 1.0f};
	sc->light.scale_factor = 0.05f;
	sc->ambient.intensity = 0.5f;
	sc->ambient.color = (t_vec3){1.0f, 1.0f, 1.0f};
	sc->gamma = 2.2;
	sc->light.light_shad_color_id =
		glGetUniformLocation(sc->light.shader_id, "lColor");
	sc->light.pos_id = glGetUniformLocation(shader_id, "lPos");
	sc->light.intensity_id = glGetUniformLocation(shader_id, "lIntensity");
	sc->light.color_id = glGetUniformLocation(shader_id, "lColor");
	sc->gamma_id = glGetUniformLocation(shader_id, "gamma");
	sc->ambient.color_id = glGetUniformLocation(shader_id, "aColor");
	sc->ambient.intensity_id = glGetUniformLocation(shader_id, "aIntensity");
}
