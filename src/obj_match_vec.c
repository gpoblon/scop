/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj_match_vec.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:16:39 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/31 16:57:30 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void	update_center(t_obj_parser *o, float x, float y, float z)
{
	if (o->min.x > x)
		o->min.x = x;
	else if (o->max.x < x)
		o->max.x = x;
	if (o->min.y > y)
		o->min.y = y;
	else if (o->max.y < y)
		o->max.y = y;
	if (o->min.z > z)
		o->min.z = z;
	else if (o->max.z < z)
		o->max.z = z;
}

void		push_vertex(t_obj_parser *o, const char *line)
{
	sscanf(
		line + 2, "%f %f %f",
		&o->r_vbo[o->vertex_ipos],
		&o->r_vbo[o->vertex_ipos + 1],
		&o->r_vbo[o->vertex_ipos + 2]);
	update_center(
		o,
		o->r_vbo[o->vertex_ipos],
		o->r_vbo[o->vertex_ipos + 1],
		o->r_vbo[o->vertex_ipos + 2]);
	o->vertex_ipos += VBO_OFFSET;
}

void		push_normal(t_obj_parser *o, const char *line)
{
	sscanf(
		line + 2, "%f %f %f",
		&o->r_vbo[o->normal_ipos],
		&o->r_vbo[o->normal_ipos + 1],
		&o->r_vbo[o->normal_ipos + 2]);
	o->normal_ipos += VBO_OFFSET;
}

void		push_texture_coord(t_obj_parser *o, const char *line)
{
	if (o->has_texture == 0)
		o->text_coord_ipos = 3;
	o->has_texture = 1;
	sscanf(
		line + 2, "%f %f %f",
		&o->r_vbo[o->text_coord_ipos],
		&o->r_vbo[o->text_coord_ipos + 1],
		&o->r_vbo[o->text_coord_ipos + 2]);
	o->text_coord_ipos += VBO_OFFSET;
}
