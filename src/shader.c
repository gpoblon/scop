/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:16:54 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/31 18:10:40 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static int	build_shader(t_uint *shader, const char *path, int shader_type)
{
	int		success;
	char	*shader_file;
	char	info_log[512];

	shader_file = string_of_file(path);
	if (!shader_file)
		return (ft_err_print("String_of_file failed"));
	*shader = glCreateShader(shader_type);
	glShaderSource(*shader, 1, (const char **)&shader_file, NULL);
	glCompileShader(*shader);
	glGetShaderiv(*shader, GL_COMPILE_STATUS, &success);
	free(shader_file);
	if (!success)
	{
		glGetShaderInfoLog(*shader, 512, NULL, info_log);
		ft_printf("ERROR::SHADER::COMPILATION_FAILED\n");
		ft_printf("%s", info_log);
		return (ft_err_print("building fragment shader failed"));
	}
	return (0);
}

int			build_shader_program(t_uint *shader_program, const char *vert_path,
			const char *frag_path)
{
	t_uint	vertex_shader;
	t_uint	fragment_shader;
	int		success;

	*shader_program = glCreateProgram();
	if (build_shader(&vertex_shader, vert_path, GL_VERTEX_SHADER) == -1)
		return (-1);
	if (build_shader(&fragment_shader, frag_path, GL_FRAGMENT_SHADER) == -1)
		return (-1);
	glAttachShader(*shader_program, vertex_shader);
	glAttachShader(*shader_program, fragment_shader);
	glLinkProgram(*shader_program);
	glGetProgramiv(*shader_program, GL_LINK_STATUS, &success);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
	if (!success)
		return (ft_err_print("Getting shader_program id failed"));
	return (0);
}

void		update_vertex_shader(t_mvp *mvp)
{
	glUniformMatrix4fv(mvp->model_id, 1, GL_TRUE, (const float *)&mvp->model);
	glUniformMatrix4fv(mvp->view_id, 1, GL_TRUE, (const float *)&mvp->view);
	glUniformMatrix4fv(mvp->proj_id, 1, GL_TRUE, (const float *)&mvp->proj);
}

void		update_fragment_color(t_scene *sc, t_vec3 *cam_pos)
{
	glUniform1i(sc->obj.has_texture_id, (int)sc->obj.has_texture);
	glUniform3fv(sc->view_pos_id, 1, (const float *)cam_pos);
	glUniform3fv(sc->light.color_id, 1, (const float *)&sc->light.color);
	glUniform3fv(sc->obj.color_id, 1, (const float *)&sc->obj.color);
	glUniform3fv(sc->light.pos_id, 1, (const float *)&sc->light.pos);
	glUniform1f(sc->light.intensity_id, sc->light.intensity);
	glUniform1f(sc->gamma_id, sc->gamma);
	glUniform3fv(sc->obj.color_id, 1, (const float *)&sc->light.color);
	glUniform3fv(sc->light.color_id, 1, (const float *)&sc->light.color);
	glUniform3fv(sc->ambient.color_id, 1, (const float *)&sc->ambient.color);
	glUniform1f(sc->ambient.intensity_id, sc->ambient.intensity);
}
