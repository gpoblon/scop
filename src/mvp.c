/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mvp.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:16:25 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/31 17:51:15 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void	camera_lookat(t_max4 *view, t_camera cam)
{
	t_vec3	tmp;

	ft_vec3sum(&tmp, cam.pos, cam.to);
	ft_maxlookat(view, cam.pos, tmp, cam.up);
}

/*
** ft_max4rotate_x(&mvp->model, (float)glfwGetTime() * -45.0f / 90);
*/

void		compute_mvp_obj(t_obj *o, t_camera cam)
{
	t_max4	max_rot;

	ft_max4identity(&max_rot);
	ft_max4identity(&o->mvp.model);
	ft_max4identity(&o->mvp.view);
	ft_max4identity(&o->mvp.proj);
	ft_max4scaled(&o->mvp.model, o->scale_factor);
	camera_lookat(&o->mvp.view, cam);
	ft_max4perspective(&o->mvp.proj, (t_perspective){ cam.fov,
		(float)WIN_WIDTH / (float)WIN_HEIGHT, 0.1f, 1000.0f });
	if (o->is_rotating)
		ft_max4rotate_y(&max_rot, (float)glfwGetTime() * -45.0f / 90);
	ft_max4mult(&o->mvp.model, max_rot);
}

void		compute_mvp_light(t_obj *o, t_camera cam)
{
	ft_max4identity(&o->mvp.model);
	ft_max4identity(&o->mvp.view);
	ft_max4identity(&o->mvp.proj);
	ft_max4scaled(&o->mvp.model, o->scale_factor);
	camera_lookat(&o->mvp.view, cam);
	ft_max4perspective(&o->mvp.proj, (t_perspective){ cam.fov,
		(float)WIN_WIDTH / (float)WIN_HEIGHT, 0.1f, 1000.0f });
	ft_vec4translate(&o->mvp.model, o->pos);
}

void		init_camera(t_camera *cam, float frame_size)
{
	cam->pos = (t_vec3){ 0.0f, 0.0f, 1.0f + frame_size };
	cam->to.x = 0.0f;
	cam->to.y = 0.0f;
	cam->to.z = 0.0f;
	cam->up = (t_vec3){ 0.0f, 1.0f, 0.0f };
	cam->first_calls = 0;
	cam->sensivity = 0.05f;
	cam->yaw = 0.0f;
	cam->pitch = 0.0f;
	cam->fov = 45.0f;
}

void		init_mvp(t_obj *obj)
{
	obj->mvp.model_id = glGetUniformLocation(obj->shader_id, "model");
	obj->mvp.view_id = glGetUniformLocation(obj->shader_id, "view");
	obj->mvp.proj_id = glGetUniformLocation(obj->shader_id, "projection");
}
