/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:17:06 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/31 12:42:05 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

float			get_frame_size(t_obj_parser o)
{
	t_vec3			len;

	len.x = o.max.x - o.min.x;
	len.y = o.max.y - o.min.y;
	return (len.x > len.y ? len.x : len.y);
}

void			realloc_vbo_if_needed(float **arr, t_uint ipos, t_uint *size,
				t_uint new_size)
{
	if (ipos > *size)
	{
		*size = new_size;
		*arr = realloc(*arr, new_size * sizeof(float));
	}
}

void			exit_proper(char *message, int fd)
{
	glfwTerminate();
	ft_exit_fd(message, fd);
}

char			*string_of_file(const char *filepath)
{
	int		fd;
	char	*file;
	char	*line;

	line = NULL;
	file = ft_strdup("");
	if ((fd = open(filepath, O_RDONLY)) < 0)
		return (NULL);
	while (get_next_line(fd, &line) > 0)
	{
		file = ft_strfjoin(file, "\n", F_1);
		file = ft_strfjoin(file, line, F_A);
	}
	return (file);
}
