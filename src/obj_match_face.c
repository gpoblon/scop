/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj_match_face.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:16:29 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/31 12:35:46 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void		reorder_vbo(t_obj_parser *o, unsigned int vbo_ipos,
	unsigned int nb_count)
{
	unsigned int	line;
	unsigned int	vert_offset;
	unsigned int	real_pos;

	line = 0;
	if (nb_count == 0)
		return ;
	while (line < nb_count - 1)
	{
		vert_offset = 0;
		while (vert_offset < VBO_OFFSET)
		{
			if (o->s_ebo[line + vert_offset / 3] == 0)
				real_pos = 0;
			else
				real_pos = (o->s_ebo[line + vert_offset / 3] - 1) * VBO_OFFSET;
			if (real_pos + vert_offset <= o->r_vbo_size)
				o->s_vbo[vbo_ipos + vert_offset] =
					o->r_vbo[real_pos + vert_offset];
			++vert_offset;
		}
		vbo_ipos += VBO_OFFSET;
		line += 3;
	}
}

static void		ints_of_line(const char *line, int *arr)
{
	int		i;
	int		j;

	i = -1;
	j = -1;
	while (line[++i])
	{
		if (ft_isdigit(line[i]))
		{
			arr[++j] = ft_atoi(line + i);
			while (line[i] && ft_isdigit(line[i]))
				++i;
			if (line[i] == ' ' || !line[i + 1])
				j = j / 3 * 3 + 2;
		}
		else if (line[i] == '/')
			++j;
	}
}

static void		triangles_of_polygones(t_obj_parser *o, int *r_ebo,
	unsigned int r_ebo_size)
{
	unsigned int	cpy_i;
	int				offset;
	int				v_type_offset;

	cpy_i = 6;
	offset = 0;
	v_type_offset = -1;
	while (cpy_i < r_ebo_size)
	{
		while (++v_type_offset < 3)
		{
			o->s_ebo[offset + 0 + v_type_offset] = r_ebo[0 + v_type_offset];
			o->s_ebo[offset + 3 + v_type_offset] =
				r_ebo[cpy_i - 3 + v_type_offset];
			o->s_ebo[offset + 6 + v_type_offset] =
				r_ebo[cpy_i + v_type_offset];
		}
		v_type_offset = -1;
		offset += 9;
		cpy_i += 3;
	}
}

static t_uint	get_ebo_size(const char *line)
{
	int		i;
	t_uint	nb_elem;

	i = -1;
	nb_elem = 0;
	while (line[++i])
	{
		if (line[i] == ' ')
			nb_elem += 3;
	}
	if (nb_elem > 0)
		nb_elem += 3;
	return (nb_elem);
}

void			push_face(t_obj_parser *o, const char *line)
{
	unsigned int	tmp_s_vbo_ipos;
	t_uint			old_ebo_size;

	old_ebo_size = o->r_ebo_size;
	o->r_ebo_size = get_ebo_size(line + 2);
	o->s_ebo_size = o->r_ebo_size >= 6 ? (9 * ((o->r_ebo_size / 3) - 2)) : 0;
	if (o->r_ebo_size != old_ebo_size)
	{
		o->r_ebo = (int *)realloc(o->r_ebo, o->r_ebo_size * sizeof(int));
		o->s_ebo = (int *)realloc(o->s_ebo, o->s_ebo_size * sizeof(int));
	}
	ft_bzero(o->r_ebo, o->r_ebo_size * sizeof(int));
	ft_bzero(o->s_ebo, o->s_ebo_size * sizeof(int));
	ints_of_line(line + 2, o->r_ebo);
	tmp_s_vbo_ipos = o->s_vbo_ipos;
	o->s_vbo_ipos += VBO_OFFSET * (o->s_ebo_size / 3);
	realloc_vbo_if_needed(&o->s_vbo, o->s_vbo_ipos, &o->s_vbo_size,
		(o->s_vbo_size + BUFFER_SIZE * o->s_ebo_size));
	triangles_of_polygones(o, o->r_ebo, o->r_ebo_size);
	reorder_vbo(o, tmp_s_vbo_ipos, o->s_ebo_size);
	return ;
}
