/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 15:06:52 by gpoblon           #+#    #+#             */
/*   Updated: 2019/01/30 15:26:43 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void	print_fl_arr(float *data, int size)
{
	int	i;

	i = -1;
	ft_printf("Array:\n");
	while (++i < size)
	{
		ft_printf(" %.2f\t", data[i]);
		if ((i + 1) % 9 == 0)
			ft_printf("\n");
	}
	ft_printf("\n");
}

void	print_int_arr(int *data, int size)
{
	int	i;

	i = -1;
	ft_printf("Array:\n");
	while (++i < size)
	{
		ft_printf(" %d\t", data[i]);
		if ((i + 1) % 3 == 0)
			ft_printf("\n");
	}
	ft_printf("\n");
}
